/*
  The theme color file
*/

const THEME_STORAGE = 'mptzThemes';

const defaultThemesCollection = [
  {
    'name': 'default', 
    'background-color': '#242424',
    'section-color': '#202020',
    'font-color': '#ffc107',
    'hightlight-color': '#ffc107',
    'grid-color': '#242424',
  },
  {
    'name': 'light',
    'background-color': '#f3f3f3',
    'section-color': '#ffffff',
    'font-color': '#4e4e4e',
    'hightlight-color': '#5ac75f',
    'grid-color': '#2e2e2e',
  },
  {
    'name': 'mint',
    'background-color': '#f5fff7',
    'section-color': '#83ffa0',
    'font-color': '#009924',
    'hightlight-color': '#009924',
    'grid-color': '#009924',
  },
  {
    'name': 'blueprint',
    'background-color': '#c7dcff',
    'section-color': '#003384',
    'font-color': '#ffffff',
    'hightlight-color': '#ffffff',
    'grid-color': '#ffffff',
  },
]

export let themesCollection = [...defaultThemesCollection];

themesCollection = getThemeFromLocal() ||  themesCollection;
initCustomTheme(0);

// -------------------------------------------------
function getThemeFromLocal() {
  const savedThemes = JSON.parse(localStorage.getItem(THEME_STORAGE));
  return savedThemes;
}

function initCustomTheme(themeIdx) {
  for (let key in themesCollection[themeIdx]) {
    if (key !== 'name') {
      const value = themesCollection[themeIdx][key]
      document.documentElement.style.setProperty(`--${key}`, `${value}`);
      
      // -- Set the popup alpha blur background color
      if (key === 'section-color') {
        document.documentElement.style.setProperty(`--section-color-alpha`, `${value}4d`);
      }
    }
  }
}

export function setThemeToLocal(newThemeCollection) {
  localStorage.setItem(THEME_STORAGE, JSON.stringify(newThemeCollection));
}

export function setDefaultTheme(themeIdx) {
  for (let key in themesCollection[themeIdx]) {
    if (key !== 'name') {
      const value = defaultThemesCollection[themeIdx][key]
      document.documentElement.style.setProperty(`--${key}`, `${value}`);
      themesCollection[themeIdx][key] = value;
      
      // -- Set the popup alpha blur background color
      if (key === 'section-color') {
        document.documentElement.style.setProperty(`--section-color-alpha`, `${value}4d`);
      }
    }
  }
  setThemeToLocal(themesCollection);
}
