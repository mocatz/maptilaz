import { writable } from 'svelte/store';

export const tileX = writable(5); // Number of tiles on X axis
export const tileY = writable(5); // Number of tiles on Y axis
export const _gridGap = writable(0); // Number of tiles on Y axis
export const _tileMap = writable([]); // Blank tile map
export const _tilesCollections = writable([]);
export const _tilesCollectionFileName = writable([]);
export const _currentMode = writable('click'); // -- 'click', 'brush', 'pipette'
export const _selectedTileIdx = writable(0);
export const _updateTileIdx = writable(null);
export const _newProject = writable(false);

// ------------------------------
// CUSTOM SETTINGS
// ------------------------------
export const _centerMap = writable(true);
export const _currentTheme = writable(0);
export const _autoSaveFile = writable(true);
export const _borderRadius = writable(0);
export const _tileCollectionSize = writable(64);
